import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BasketService } from 'src/app/shared/services/basket/basket.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  feature: string = ''
	count: number = 0

	basketCountSubscription: Subscription

  constructor(private router: Router, private basketService: BasketService) { }

  ngOnInit(): void {
		this.basketCountSubscription = this.basketService.getBasketCount().subscribe(count => {
			this.count = count
		})
  }

	/**
	 * @author Adam Gilmour
	 * @description When a menu item has been clicked it will route it to the correct route.
	 * @param $event Menu click event
	 */
	onSelect($event: string) {
		this.router.navigate([$event])
	}
}

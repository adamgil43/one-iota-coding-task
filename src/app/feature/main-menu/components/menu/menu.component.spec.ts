import { ComponentFixture, discardPeriodicTasks, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';

import { MenuComponent } from './menu.component';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;
	
	/**
	 * @description a spy object for the router service class
	 */
	let mockRouter = jasmine.createSpyObj<Router>('Router', ['navigate'])

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuComponent ],
			providers: [
				{ provide: Router, useValue: mockRouter },
			]
    })
    .compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
		flush()
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	describe('navigiation', () => {
		/**
		 * @author Adam Gilmour
		 * @describe Tests that when the Product has been clicked on the menu that the router navigates to the products route.
		 * This is tested by checking that the navigate function from the router service is called with products.
		 */
		it('should verify that when the products menu select is clicked, that the router navigates to that route', fakeAsync(() => {
			const el: HTMLElement = fixture.nativeElement
			const homeLink: HTMLAnchorElement  = el.querySelector('#product-menu-select') as HTMLAnchorElement
			homeLink.dispatchEvent(new Event('click'))
			tick()
			fixture.detectChanges()
			flush()
			expect(mockRouter.navigate).toHaveBeenCalledWith(['products'])
			discardPeriodicTasks()
		}))

		/**
		 * @author Adam Gilmour
		 * @describe Tests that when the Basket has been clicked on the menu that the router navigates to the basket route.
		 * This is tested by checking that the navigate function from the router service is called with basket.
		 */
		it('should verify that when the basket is clicked, that the router navigates to that route', fakeAsync(() => {
			const el: HTMLElement = fixture.nativeElement
			const homeLink: HTMLAnchorElement  = el.querySelector('#basket-menu-select') as HTMLAnchorElement
			homeLink.dispatchEvent(new Event('click'))
			tick()
			fixture.detectChanges()
			flush()
			expect(mockRouter.navigate).toHaveBeenCalledWith(['basket'])
			discardPeriodicTasks()
		}))
	})
});

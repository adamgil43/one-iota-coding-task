import { Component, Input, OnInit } from '@angular/core';
import { ProductBasket } from 'src/app/shared/interface/basket/basket.interface';

@Component({
  selector: 'app-basket-item',
  templateUrl: './basket-item.component.html',
  styleUrls: ['./basket-item.component.css']
})
export class BasketItemComponent implements OnInit {
	@Input() basket: ProductBasket

	price: number

	selectedSizes: string

  constructor() { }

  ngOnInit(): void {
		if (this.basket) {
			const price = parseFloat(this.basket.price.amount)
			const quantity = this.basket.quantity
			this.price = price * quantity
	
			this.selectedSizes = this.basket.sizes.join(', ')
		}
  }

}

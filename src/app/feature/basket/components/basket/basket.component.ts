import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProductBasket } from 'src/app/shared/interface/basket/basket.interface';
import { BasketService } from 'src/app/shared/services/basket/basket.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit, OnDestroy {
	basketServiceSubscription: Subscription

	basketList: ProductBasket[]

	subtotal: number

  constructor(private basketService: BasketService) { }

  ngOnInit(): void {
		this.basketServiceSubscription = this.basketService.getBasket().subscribe((item: ProductBasket[]) => {
			this.basketList = item

			const totals = this.basketList.map(element => {
				const price = parseFloat(element.price.amount)
				const quantity = element.quantity

				return price * quantity
			})
			this.subtotal = totals.reduce((previous, next) => previous + next, 0)
		})
  }

	ngOnDestroy(): void {
		this.basketServiceSubscription.unsubscribe()	
	}

}

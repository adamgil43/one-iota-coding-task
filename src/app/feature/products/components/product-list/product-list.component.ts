import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ProductAPIResponse } from 'src/app/shared/models/product/product.interface';
import { ProductsService } from 'src/app/shared/services/products/products.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
	@Output() onProductSelected = new EventEmitter<ProductAPIResponse>()
	products: ProductAPIResponse[] = []

  constructor(private productService: ProductsService) { }

  ngOnInit(): void {
		this.#refreshData()
  }

	/**
	 * @author Adam Gilmour
	 * @description When a product is clicked this function will run and emit a event to the parent for further processing.
	 * @param $event The product response
	 */
	onProductClicked($event: ProductAPIResponse) {
		this.onProductSelected.emit($event)
	}

	/**
	 * @author Adam Gilmour
	 * @description Refreshses the data and sets the response.
	 */
	async #refreshData() {
		const response = await this.productService.getProductList()
		if (response) {
			this.products = response
		}
	}

}

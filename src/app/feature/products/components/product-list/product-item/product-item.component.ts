import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductAPIResponse } from 'src/app/shared/models/product/product.interface';
import { RecentlyViewedService } from 'src/app/shared/services/recently-viewed/recently-viewed.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {
  @Input() product: ProductAPIResponse

  @Output() onProductSelected: EventEmitter<ProductAPIResponse> = new EventEmitter<ProductAPIResponse>()

  constructor(private recentlyViewService: RecentlyViewedService) { }

  ngOnInit(): void {
  }

	onProductClick() {
		this.recentlyViewService.updatePreviousList(this.product)
		this.onProductSelected.emit(this.product)
	}
}

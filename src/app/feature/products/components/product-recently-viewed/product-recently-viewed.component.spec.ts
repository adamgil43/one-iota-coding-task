import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductAPIResponse } from 'src/app/shared/models/product/product.interface';
import { RecentlyViewedService } from 'src/app/shared/services/recently-viewed/recently-viewed.service';
import { ProductRecentlyViewedComponent } from './product-recently-viewed.component';

describe('ProductRecentlyViewedComponent', () => {
  let component: ProductRecentlyViewedComponent;
  let fixture: ComponentFixture<ProductRecentlyViewedComponent>;
	let recentlyViewedService: RecentlyViewedService

	let fakeResponse: ProductAPIResponse = {
		"id": "1",
		"SKU": "1210",
		"name": "Nike Air Relentless 4 Mens Running Shoes",
		"brandName": "Nike",
		"mainImage": "https://s3-eu-west-1.amazonaws.com/api.themeshplatform.com/media/7e386191b2ee40b290886a05d3e10e24_nike-air-relentless-a.jpg",
		"price": {
			"amount": "45.00",
			"currency": "GBP"
		},
		"sizes": [
			"8",
			"9",
			"10",
			"11"
		],
		"stockStatus": "IN STOCK",
		"colour": "blue",
		"description": "Hit the tracks in these Nike Air Relentless 4 featuring flexible forefoot sole and Reslon midsole underfoot cushioning for superior comfort at each step. The ridged outsole ensures excellent traction while the cushioned ankle collar and the anatomically shaped insole guarantee great support for the whole foot. The mesh upper panels provide breathability and airflow within the shoe."
	}
	
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductRecentlyViewedComponent ],
			providers: [
				{ provider: RecentlyViewedService, useValue: recentlyViewedService }
			]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductRecentlyViewedComponent);
		recentlyViewedService = TestBed.inject(RecentlyViewedService)
    component = fixture.componentInstance;
		recentlyViewedService.updatePreviousList(fakeResponse)
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

	it('should expect the previousProducts to have a length of 1', () => {
		expect(component.previousProducts.length).toEqual(1)
	})

	it('should expect the previousProducts to have a length of 2', () => {
		recentlyViewedService.updatePreviousList(fakeResponse)
		expect(component.previousProducts.length).toEqual(2)
	})
});

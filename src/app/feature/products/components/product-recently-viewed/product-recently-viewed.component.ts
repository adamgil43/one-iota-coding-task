import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProductAPIResponse } from 'src/app/shared/models/product/product.interface';
import { RecentlyViewedService } from 'src/app/shared/services/recently-viewed/recently-viewed.service';

@Component({
  selector: 'app-product-recently-viewed',
  templateUrl: './product-recently-viewed.component.html',
  styleUrls: ['./product-recently-viewed.component.css']
})
export class ProductRecentlyViewedComponent implements OnInit, OnDestroy {
	previousProducts: ProductAPIResponse[] = []
	recentlyViewedSubscription: Subscription

  constructor(private recentlyViewedService: RecentlyViewedService) { }

	/**
	 * @author Adam Gilmour
	 * @description Sets up the subscription that this component relys upon. The subscription controls what product
	 * has recently been select and returns it as a list.
	 */
  ngOnInit(): void {
		this.recentlyViewedSubscription = this.recentlyViewedService.getPreviousListObservable().subscribe((current: ProductAPIResponse[]) => {
			this.previousProducts = current
		})
  }

	/**
	 * @author Adam Gilmour
	 * @description unsubscribes from the subscription to prevent memory leaks.
	 */
	ngOnDestroy(): void {
		this.recentlyViewedSubscription.unsubscribe()
	}

}

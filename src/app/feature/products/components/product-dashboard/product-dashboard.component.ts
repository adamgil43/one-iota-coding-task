import { Component, OnInit } from '@angular/core';
import { ProductAPIResponse } from 'src/app/shared/models/product/product.interface';

@Component({
  selector: 'app-product-dashboard',
  templateUrl: './product-dashboard.component.html',
  styleUrls: ['./product-dashboard.component.css']
})
export class ProductDashboardComponent implements OnInit {
	productSelected: ProductAPIResponse
  constructor() { }

  ngOnInit(): void {
  }

	/**
	 * @author Adam Gilmour
	 * @description This is another way to send data between children, by sending the data from the child back to the parent and
	 * the parent then sends it down again. This response will event be sent to the product view.
	 * @param $event The clicked product from the child
	 */
	onProductSelected($event: ProductAPIResponse) {
		this.productSelected = $event
	}

}

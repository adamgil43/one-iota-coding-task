import { Component, Input, OnInit } from '@angular/core';
import { ProductAPIResponse } from 'src/app/shared/models/product/product.interface';
import { BasketService } from 'src/app/shared/services/basket/basket.service';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {
	@Input() productSelected: ProductAPIResponse

	selectedSize = 'Select Shoe Size'

	disableAddToBasket = true

	#shoeSize: string
	
  constructor(private basketService: BasketService) { }

  ngOnInit(): void {
  }

	/**
	 * @author Adam Gilmour
	 * @description Sets the selected shoe size to two different values, one that is present in the DOM and the other that is
	 * used by the add to basket function.
	 * @param size The size of the shoe
	 */
	selectShoeSize(size: string) {
		this.selectedSize = `Shoe size: ${size}`
		this.#shoeSize = size
		this.disableAddToBasket = false
	}

	/**
	 * @author Adam Gilmour
	 * @description Adds a product to the basket.
	 * This function will add a product the basket if a shoe size has been selected as that shoe size is in the show size list.
	 */
	addToBasket() {
		if (this.productSelected.sizes.includes(this.#shoeSize) && !this.disableAddToBasket) {
			this.basketService.addToBasket(this.productSelected, this.#shoeSize)
		}
	}

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductDashboardComponent } from './components/product-dashboard/product-dashboard.component';
import { ProductsRoutingModule } from './products-routing';
import { ProductItemComponent } from './components/product-list/product-item/product-item.component';
import { HoverModule } from 'src/app/shared/directives/hover/hover.module';
import { ProductRecentlyViewedComponent } from './components/product-recently-viewed/product-recently-viewed.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    ProductViewComponent,
    ProductListComponent,
    ProductDashboardComponent,
    ProductItemComponent,
    ProductRecentlyViewedComponent
  ],
  imports: [
    CommonModule,
		ProductsRoutingModule,
		HoverModule,
		NgbModule
  ],
	exports: [
		ProductRecentlyViewedComponent
	]
})
export class ProductsModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{ path: '', loadChildren: () => import('./feature/products/products.module').then(x => x.ProductsModule) },
	{ path: 'products', loadChildren: () => import('./feature/products/products.module').then(x => x.ProductsModule) },
	{ path: 'basket', loadChildren: () => import('./feature/basket/basket.module').then(x => x.BasketModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HoverDirective } from './hover.directive';

@Component({
	selector: 'test-component',
	template: '<div class="card cursor-pointer" id="test" appHover></div>'
})
class TestComponent {
}
describe('HoverDirective', () => {
	let directive: HoverDirective
	let fixture: ComponentFixture<TestComponent>
	
	/**
	 * @description generate the mock module used for testing and compiles the necessary components
	 */
	beforeEach(async() => {
		await TestBed.configureTestingModule({
			declarations: [ TestComponent, HoverDirective ],
		}).compileComponents()
	})

	beforeEach(() => {
		fixture = TestBed.createComponent(TestComponent)
		fixture.detectChanges()
	})

	/**
	 * @author Adam Gilmour
	 * @description initialisation of the directive, this always pass as long as there are no fundamental errors in the directive class.
	 */
  it('should create an instance', () => {
    directive = new HoverDirective();
    expect(directive).toBeTruthy();
  });

	/**
	 * @author Adam Gilmour
	 * @description Testing that the mouse enter causes the grey boarder to appear. This is tested by triggering a mouse enter
	 * event on the element and testing that a new class is added to the elements class list.
	 */
	it('should test that a mouse enter makes the class list to contain grey-border', () => {
		const el: HTMLElement = fixture.nativeElement
		const mouseHoverEnter: HTMLDivElement = el.querySelector('#test') as HTMLDivElement
		mouseHoverEnter.dispatchEvent(new MouseEvent('mouseenter'))
		fixture.detectChanges()
		expect(mouseHoverEnter.classList).toContain('blue-border')
	})

	/**
	 * @author Adam Gilmour
	 * @description Testing that the mouse enter and then mouse leavecauses the grey boarder to disappear. This is testing in two parts,
	 * by triggering the mouse enter event which is exactly the test above and then the mouse leave and checking the class list
	 * to make sure that the grey-border has left the list.
	 */
	it('should test that a mouse enter and leave makes the grey border leave the class list', () => {
		const el: HTMLElement = fixture.nativeElement
		const mouseHoverEnter: HTMLDivElement = el.querySelector('#test') as HTMLDivElement
		mouseHoverEnter.dispatchEvent(new MouseEvent('mouseenter'))
		fixture.detectChanges()
		mouseHoverEnter.dispatchEvent(new MouseEvent('mouseleave'))
		fixture.detectChanges()
		expect(mouseHoverEnter.classList).not.toContain('blue-border')
	})

});

export interface ProductBasket {
	'SKU': string,
	'name': string,
	'price': {
			"amount": string,
			"currency": string
	},
	'quantity': number,
	'mainImage': string,
	'sizes': string[]
}
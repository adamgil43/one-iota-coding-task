import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProductAPIResponse } from '../../models/product/product.interface';

@Injectable({
  providedIn: 'root'
})
export class RecentlyViewedService {
	private previousArray: ProductAPIResponse[] = []

	private previousListSource = new BehaviorSubject<ProductAPIResponse[]>(this.previousArray)

	private previousListObservable$ = this.previousListSource.asObservable()
	
	/**
	 * @author Adam Gilmour
	 * @description Used to update the previous list.
	 * @param item The prodcut that was last viewed
	 */
	updatePreviousList(item: ProductAPIResponse) {
		this.previousArray.push(item)
		this.previousListSource.next(this.previousArray)
	}

	/**
	 * @author Adam Gilmour
	 * @describe Used to access the list that has been set in the observable.
	 * @returns {Observable<ProductAPIResponse[]>} Previous List Observable
	 */
	getPreviousListObservable(): Observable<ProductAPIResponse[]> {
		return this.previousListObservable$
	}
}

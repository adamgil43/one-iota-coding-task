import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProductBasket } from '../../interface/basket/basket.interface';
import { ProductAPIResponse } from '../../models/product/product.interface';

@Injectable({
  providedIn: 'root'
})
export class BasketService {
	private basketList: ProductBasket[] = []

	private basketListSource = new BehaviorSubject<ProductBasket[]>([])

	private basketListObservable$ = this.basketListSource.asObservable()

	private basketCountSource =new BehaviorSubject<number>(0)

	private basketCountObservable$ = this.basketCountSource.asObservable()

  constructor() { }

	/**
	 * @author Adam Gilmour
	 * @description This function adds new items to the basket.
	 * This is done by finding out if the product first exists; if it does then we just update the quantity and size,
	 * if not we create a new product that will contain the new product.
	 * We use the SKU to compare if a product has already been added.
	 * @param item the item added to the basket
	 * @param size The shoe size that has been added
	 */
	addToBasket(item: ProductAPIResponse, size: string) {
		const productSKU = item.SKU
		const itemIndex = this.basketList.findIndex(element => element.SKU === productSKU)
		if (itemIndex === -1) {
			const product: ProductBasket = {
				'SKU': item.SKU,
				'name': item.name,
				'price': item.price,
				'quantity': 1,
				'mainImage': item.mainImage,
				'sizes': [size]
			}
			this.basketList.push(product)
		} else {
			this.basketList[itemIndex].quantity += 1
			this.basketList[itemIndex].sizes.push(size)
		}
		const quantityArry: number[] = this.basketList.map(element => element.quantity)
		const basketCount = quantityArry.reduce((previousValue, nextValue) => previousValue + nextValue, 0)

		this.basketCountSource.next(basketCount)
		this.basketListSource.next(this.basketList)
	}

	/**
	 * @author Adam Gilmour
	 * @description Gets the basket observable.
	 * @returns {Observable<ProductBasket[]>} The basket observable
	 */
	getBasket(): Observable<ProductBasket[]> {
		return this.basketListObservable$
	}

	/**
	 * @author Adam Gilmour
	 * @description Gets the basket count observable.
	 * @returns {Observable<number>} The basket count observable
	 */
	getBasketCount(): Observable<number> {
		return this.basketCountObservable$
	}
}

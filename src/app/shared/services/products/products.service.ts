import { Injectable } from '@angular/core';
import axios from 'axios';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProductAPIResponse } from '../../models/product/product.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
	/**
	 * @author Adam Gilmour
	 * @description Gets the product list from the API.
	 * The API response is in the form of a JSON object, which you can deconstruct the product data from. The module provided makes
	 * sure that the data coming from the API is correct, if something is missing it will fail and will catch the error.
	 * This makes sure that invalid responses does not make it past this stage.
	 * @returns {Promise<ProductsApiResponse[]|false>} API response for the product list or false if something has gone wrong.
	 */
	async getProductList(): Promise<ProductAPIResponse[]|false> {
		try {
			const { data }: { data: any } = await axios.get(`https://s3-eu-west-1.amazonaws.com/api.themeshplatform.com/products.json`)
			const response: ProductAPIResponse[] = data.data

			return response
		} catch (ex) {

			return false
		}
	}
}

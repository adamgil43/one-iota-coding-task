/**
 * An interface used by the API that is used to get the products.
 */
export interface ProductAPIResponse  {
	"id": string,
	"SKU": string,
	"name": string,
	"brandName": string,
	"mainImage": string,
	"price": {
			"amount": string,
			"currency": string
	},
	"sizes": string[],
	"stockStatus": string,
	"colour": string,
	"description": string
}
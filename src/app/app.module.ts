import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MainMenuModule } from './feature/main-menu/main-menu.module';
import { ProductsModule } from './feature/products/products.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
		MainMenuModule,
		ProductsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
